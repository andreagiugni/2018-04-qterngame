<?php
function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
       $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}
$seconds=$_GET['seconds'];
$ipaddress=get_client_ip();
$now=date("Y-m-d H:i:s");
try{
    $mysqli = new mysqli('localhost', 'root', 'root', 'qterngame_db');
    $query=$mysqli->prepare("INSERT INTO user_record (seconds, date, ipaddress) VALUES('".$seconds."', '".$now."', '".$ipaddress."')");
    $query->execute();
}catch(mysqli_sql_exception $e){
    print "Error!: " . $e->errorMessage() . "<br/>";
    die();
}
?>