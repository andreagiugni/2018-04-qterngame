var game = new Phaser.Game(window.innerWidth,window.innerHeight, Phaser.AUTO, 'phaser-demo', {preload: preload, create: create, update: update, render: render});
var ACCLERATION = 700;
var DRAG = 800;
var MAXSPEED = 600;
var defaultImgWidth = 1920;
var defaultImgHeight = 1080;
var due;
var poly;
var graphics;
var player;
var land = new Array(4);
var blackWall = new Array(7);
var blackWallVirtual = new Array(7);
var cursors;
var bank;
var pi = 0;
var mainVelocity = 2;
var initialVelocity;
var endAnimation = false;
var imgHeight = window.innerHeight;
var imgWidth =  defaultImgWidth*imgHeight/defaultImgHeight;
var animate = false;
var items, walls;
var debug=false;
var powerTimer=new Timer({precision: 'seconds'});
var powerAvailable=true;
var loadingPower=false;
var explosions;
var collided=false;
var collisionAlert=false;
var powerAudio, standardAudio, alertAudio, explosionAudio;
var playerEmitterTop, playerEmitterBottom;
var endedGame=false;
var arms=[];
var pipes=[];
function preload() {
    for(var i=0;i<land.length;i++){
        game.load.image('land-'+(i+1), 'assets/img/land/land-'+(i+1)+'.jpg');
    }
    for(var i=0;i<blackWall.length;i++){
        game.load.image('wall-'+(i+1)+'-top', 'assets/img/blackwall/wall-'+(i+2)+'-top.png');
        game.load.image('wall-'+(i+1)+'-bottom', 'assets/img/blackwall/wall-'+(i+2)+'-bottom.png');
    }
    game.load.audio('standard', 'assets/audio/Standard.mp3');
    game.load.audio('power', 'assets/audio/PowerUp.mp3');
    game.load.audio('alert', 'assets/audio/RedAlert.mp3');
    game.load.audio('explosion', 'assets/audio/Explosion.mp3');

    game.load.image('ship', 'assets/img/player.png');
    game.load.image('shipvirtual', 'assets/img/player.png');
    game.load.image('shipvirtualcollision', 'assets/img/player.png');
    game.load.image('item', 'assets/img/item.png');
    game.load.image('wall-half-virtual', 'assets/img/blackwall/wall-half-virtual.png');
    game.load.image('particles', 'assets/img/particles.png');
    game.load.image('arm-left', 'assets/img/arm-left.png');
    game.load.image('pipe', 'assets/img/pipe.png');
    
    game.load.spritesheet('kaboom', 'assets/img/explode.png', 320, 320);
    game.load.physics('physicsData', 'punti.json');
    game.paused=true;
}
function scaleLand(){
    for(var i=0;i<land.length; i++){
        land[i].scale.setTo(imgWidth/defaultImgWidth, imgHeight/defaultImgHeight);
    }
}
function scaleBlackWall(){
    for(var i=0;i< blackWall.length; i++){
        blackWall[i][0].scale.setTo(imgWidth/defaultImgWidth, imgHeight/defaultImgHeight);
        blackWall[i][1].scale.setTo(imgWidth/defaultImgWidth, imgHeight/defaultImgHeight);
        blackWallVirtual[i][0].scale.setTo(imgWidth/defaultImgWidth, imgHeight/defaultImgHeight);
        blackWallVirtual[i][1].scale.setTo(imgWidth/defaultImgWidth, imgHeight/defaultImgHeight);
    }
}
function decrementVelocity(){
    
}

function endGame(){
    mainVelocity=0;
   
    timer.pause();
    powerTimer.stop();
    //game.add.tween(player).to( { x: game.width-300, y: game.height/2,angle:'+90'}, 1200, Phaser.Easing.Linear.None, true);
    MAXSPEED = 10000;
    
    game.physics.arcade.moveToXY(player, game.width-270,game.height/4,50,1000);
    if(player.x>=game.width-300)
    {
        if(player.angle>-90){
            player.angle -= 1.8;
        }
        else{
            game.physics.arcade.moveToXY(player, game.width-270, game.height/2 + 130, 50,1500);
        }
        if(player.y<=(game.height/2+100)){
            playerEmitterTop.emitParticle();
            playerEmitterBottom.emitParticle();
        }
        else{
            
            //game.paused=true;
            setTimeout(function(){
                game.paused=true;
                finalScreenResult();
            },1000);
            
        }
    }
    else
    {
        playerEmitterTop.emitParticle();
        playerEmitterBottom.emitParticle();
    }
    $(".power-loading span").stop();
    $(".power-box .box-icon").removeClass("enabled");
    endedGame=true;
}
function moveBackGround(xVelocity){
    if(land[land.length-1].x <= game.width/2){
        mainVelocity=mainVelocity*0.9995;
        if(land[land.length-1].x<=0){
            endGame();
        }
    }
    for(var i=0;i<land.length;i++){
        /*CHECK IF END POSITION*******/
        if((player.body.acceleration.x)>0){
            
            land[i].x = land[i].x - (xVelocity*(player.body.acceleration.x/200));
        }
        else{
            land[i].x-=xVelocity;
        }
    }
}
function moveBlackWall(xVelocity){
    
    for(var i=0;i<blackWall.length;i++){
        if((player.body.acceleration.x)>0){
            blackWall[i][0].x = blackWall[i][0].x - (xVelocity*(player.body.acceleration.x/200));
            blackWall[i][1].x = blackWall[i][1].x - (xVelocity*(player.body.acceleration.x/200));
        }
        else{
            blackWall[i][0].x-=xVelocity;
            blackWall[i][1].x-=xVelocity;
            
        }
        blackWallVirtual[i][0].body.x=blackWall[i][0].body.x + blackWall[i][0].body.width/2;
        blackWallVirtual[i][1].body.x=blackWall[i][1].body.x + blackWall[i][1].body.width/2;
    }
}
function moveItems(xVelocity){
    for(var i=0;i<items.children.length;i++){
        if((player.body.acceleration.x/100)>0){
            items.children[i].body.x = items.children[i].body.x - (xVelocity*(player.body.acceleration.x/200));
        }
        else
        {
            items.children[i].body.x-=xVelocity;
        }
        
        items.children[i].body.rotation-=(xVelocity*0.015);
    }
    for(var i=0;i<arms.length;i++){
        if((player.body.acceleration.x/100)>0){
            arms[i].body.x = arms[i].body.x - (xVelocity*(player.body.acceleration.x/200));
        }
        else
        {
            arms[i].body.x-=xVelocity;
        }        
    }
    if(land[land.length-2].x<700){
        //items.children[0].body.static=false;
        items.children[items.children.length-9].body.velocity.y=100;
        items.children[items.children.length-9].body.velocity.x=-150;
    }
    if(land[land.length-1].x<400){
        //items.children[0].body.static=false;
        items.children[items.children.length-3].body.velocity.y=100;
    }
}
function create(){
    
    game.physics.startSystem(Phaser.Physics.P2JS);
    var playerCollisionGroup = game.physics.p2.createCollisionGroup();
    var itemCollisionGroup = game.physics.p2.createCollisionGroup();
    var wallCollisionGroup = game.physics.p2.createCollisionGroup();

    /********************INIT AUDIO*****************/
    /* standardAudio = game.add.audio('standard');
    powerAudio = game.add.audio('power');
    explosionAudio = game.add.audio('explosion');
    alertAudio = game.add.audio('alert');
    alertAudio.allowMultiple=true;
    powerAudio.allowMultiple=true; */
    game.physics.p2.setImpactEvents(true);
    for(var i=0;i<land.length;i++){
        land[i] = game.add.sprite(imgWidth*i,0,"land-"+(i+1)); 
    }
    for(var i=0;i<blackWall.length;i++){
        //blackWall[i] = game.add.sprite(imgWidth*i,0,"wall-"+(i+1)); 
        blackWall[i]=new Array(2);
        blackWallVirtual[i]=new Array(2);
        blackWall[i][0] = game.add.sprite(imgWidth*i,0,"wall-"+(i+1)+"-top"); 
        blackWall[i][1] = game.add.sprite(imgWidth*(i),540,"wall-"+(i+1)+"-bottom");
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.physics.arcade.enable(blackWall[i][0], false);
        game.physics.arcade.enable(blackWall[i][1], false);
        blackWallVirtual[i][0] = game.add.sprite(imgWidth*i+(blackWall[i][0].body.width/2),blackWall[i][0].body.height/2,"wall-half-virtual");
        blackWallVirtual[i][1] = game.add.sprite(imgWidth*i+(blackWall[i][1].body.width/2),blackWall[i][1].body.height*1.5,"wall-half-virtual");
        
        game.physics.startSystem(Phaser.Physics.P2JS);
        game.physics.p2.enable(blackWallVirtual[i][0], debug);
        game.physics.p2.enable(blackWallVirtual[i][1], debug);
        blackWallVirtual[i][0].body.static=true;
        blackWallVirtual[i][1].body.static=true;
        blackWall[i][0].anchor.setTo(0, 0);
        blackWall[i][1].anchor.setTo(0, 0);
        blackWallVirtual[i][0].anchor.setTo(0, 0);
        blackWallVirtual[i][1].anchor.setTo(0, 0);
        blackWallVirtual[i][0].body.clearShapes();
        blackWallVirtual[i][1].body.clearShapes();
        traslatePolygon('physicsData', 'physicsDataTop'+i, "wall-"+(i+2)+"-top", i);
        traslatePolygon('physicsData', 'physicsDataBottom'+i, "wall-"+(i+2)+"-bottom", i);
        blackWallVirtual[i][0].body.loadPolygon('physicsDataTop'+i, "wall-"+(i+2)+"-top");
        blackWallVirtual[i][1].body.loadPolygon('physicsDataBottom'+i, "wall-"+(i+2)+"-bottom");
        blackWallVirtual[i][0].body.setCollisionGroup(wallCollisionGroup);
        blackWallVirtual[i][0].body.collides(playerCollisionGroup, collision, this);
        blackWallVirtual[i][1].body.setCollisionGroup(wallCollisionGroup);
        blackWallVirtual[i][1].body.collides(playerCollisionGroup, collision, this);
    } 
    scaleLand();
    scaleBlackWall();
    player = game.add.sprite(150, game.height*0.6, 'ship');
    playerVirtual = game.add.sprite(150, game.height*0.6, 'shipvirtual');
    playerVirtualCollision = game.add.sprite(150, game.height*0.6, 'shipvirtualcollision');
    game.physics.startSystem(Phaser.Physics.ARCADE);
    game.physics.arcade.enable(player, false);
    player.body.maxVelocity.setTo(MAXSPEED, MAXSPEED);
    player.body.drag.setTo(DRAG, DRAG);
    player.scale.setTo(0.7,0.7);
    game.physics.startSystem(Phaser.Physics.P2JS);
    playerVirtual.scale.setTo(0.7,0.7);
    playerVirtualCollision.scale.setTo(1.1,1.1);
    game.physics.p2.enable(playerVirtual, debug);
    game.physics.p2.enable(playerVirtualCollision, debug);
    playerVirtual.body.clearShapes();
    playerVirtualCollision.body.clearShapes();
    scalePolygon('physicsData', 'physicsDataRes', 'playerVirtual', 0.7);
    scalePolygon('physicsData', 'physicsDataResCollision', 'playerVirtual', 1.1);
    playerVirtual.body.loadPolygon('physicsDataRes', 'playerVirtual');
    playerVirtualCollision.body.loadPolygon('physicsDataResCollision', 'playerVirtual');

    /********************SETTING COLLISION GROUP ***************/
    game.physics.p2.updateBoundsCollisionGroup();
    items = game.add.group();
    walls = game.add.group();
    for(var i=0; i<itemPositions.length;i++){
        for(var j=0;j<itemPositions[i].length;j++){
            /***ADD ARMS**/
            if((i==2 && j==2) || (i==3 && j==1)|| (i==4 && j==2) || (i==6 && j==1)){
                
                game.physics.startSystem(Phaser.Physics.ARCADE);
                if(itemPositions[i][j].y<500)
                {
                    var arm = game.add.sprite(itemPositions[i][j].x+(i*game.width)-(itemPositions[i][j].scale*50*1.14), itemPositions[i][j].y-(100*itemPositions[i][j].scale*2.12), "arm-left");
                    game.physics.arcade.enable(arm, debug);
                    arm.angle=-180;
                }
                else
                {
                    var arm = game.add.sprite(itemPositions[i][j].x+(i*game.width)+(itemPositions[i][j].scale*50*1.14), itemPositions[i][j].y+(100*itemPositions[i][j].scale*2.12), "arm-left");
                    game.physics.arcade.enable(arm, debug);
                    if(i==6){
                        arm.x=itemPositions[i][j].x+(i*game.width)-(itemPositions[i][j].scale*56);
                        arm.angle=30;
                    }
                }
                arm.anchor.setTo(0.5,0.5);
                arm.scale.setTo(itemPositions[i][j].scale*1.40, itemPositions[i][j].scale*1.40);
                arm.body.static=true;
                arms.push(arm);
                game.physics.startSystem(Phaser.Physics.P2JS);
            }
            item = items.create(itemPositions[i][j].x+(i*game.width), itemPositions[i][j].y, "item");
            
            item.scale.setTo(itemPositions[i][j].scale, itemPositions[i][j].scale);
            game.physics.p2.enable(item, debug);
            item.body.clearShapes();
            item.body.setCircle(item.width/2);
            item.body.static=true;
            item.body.setCollisionGroup(itemCollisionGroup);
            item.body.collides(playerCollisionGroup, collision, this);
        }
    }
    playerVirtual.body.setCollisionGroup(playerCollisionGroup);
    playerVirtualCollision.body.setCollisionGroup(playerCollisionGroup);
    playerVirtual.body.collides(itemCollisionGroup, collision, this);
    playerVirtual.body.collides(wallCollisionGroup, collision, this);
    playerVirtual.alpha=0;
    playerVirtualCollision.body.collides(itemCollisionGroup, collision, this);
    playerVirtualCollision.body.collides(wallCollisionGroup, collision, this);
    playerVirtualCollision.alpha=0;

    //  And some controls to play the game with
    cursors = game.input.keyboard.createCursorKeys(); 
    button = game.input.keyboard.addKeys({'q': Phaser.KeyCode.Q});
    player.anchor.setTo(0.5, 0.5);
    playerVirtual.anchor.setTo(0.5,0.5); 
    playerVirtualCollision.anchor.setTo(0.5,0.5); 

    /**********************SETTING EXPLOSION ANIMATION ****/
    explosions = game.add.group();
    explosions.createMultiple(50, 'kaboom');
    explosions.forEach(setupInvader, this);

    /***********************SETTING PARTICLES EMITTER ************/
    playerEmitterTop = game.add.emitter(0, 0, 250);
    playerEmitterBottom = game.add.emitter(0, 0, 250);
    playerEmitterTop.alpha=0.5;
    playerEmitterBottom.alpha=0.5;
    playerEmitterTop.makeParticles('particles');
    playerEmitterBottom.makeParticles('particles');
    // Attach the emitter to the sprite
    player.addChild(playerEmitterTop);
    player.addChild(playerEmitterBottom);
    //position the emitter relative to the sprite's anchor location
    playerEmitterTop.y = -16;
    playerEmitterTop.x = -116;

    playerEmitterBottom.y = 16;
    playerEmitterBottom.x = -116;
    
    // setup options for the emitter
    playerEmitterTop.lifespan = 500;
    playerEmitterBottom.lifespan = 500;
    playerEmitterTop.maxParticleSpeed = new Phaser.Point(-10,0);
    playerEmitterTop.minParticleSpeed = new Phaser.Point(-350,120);

    playerEmitterBottom.maxParticleSpeed = new Phaser.Point(-10,0);
    playerEmitterBottom.minParticleSpeed = new Phaser.Point(-350,-120);
    
}
function setupInvader (invader){
    invader.anchor.x = 0.2;
    invader.anchor.y = 0.5;
    invader.animations.add('kaboom');
}
function update() {
    player.body.acceleration.x = 0;
    player.body.acceleration.y = 0;
    
    if (cursors.left.isDown || due=="LEFT")
    {
        player.body.acceleration.x = -ACCLERATION*0.7;
        playerEmitterTop.emitParticle();
        playerEmitterBottom.emitParticle();
    }
    else if (cursors.right.isDown || due=="RIGHT")
    {
        player.body.acceleration.x = ACCLERATION*0.7;
        playerEmitterTop.emitParticle();
        playerEmitterBottom.emitParticle();
    }
    if (cursors.up.isDown || due=="UP")
    {
        player.body.acceleration.y = -ACCLERATION;
        playerEmitterTop.emitParticle();
        playerEmitterBottom.emitParticle();
    }
    else if (cursors.down.isDown || due=="DOWN")
    {
        player.body.acceleration.y = ACCLERATION;
        playerEmitterTop.emitParticle();
        playerEmitterBottom.emitParticle();
    }
    if(button.q.isDown){
        powerActivate();
    }
    bank = player.body.velocity.y / MAXSPEED;
    if(!endedGame){
        player.angle = bank * 65;
        //  Stop at screen edges
        if (player.x > 150) {
            player.x = 150;
        }
        if (player.x < 150) {
            player.x = 150;
            player.body.acceleration.x = 0;
        }    
        if (player.y > game.height - 150) {
            player.y = game.height - 150;
            //player.body.acceleration.y = 0;
        }
        if (player.y < 150) {
            player.y = 150;
            player.body.acceleration.y = 0;
        }
    }
    
    /*************VIRTUAL SHIP HAVE TO FOLLOW THE REAL ONE */
    playerVirtual.body.x=player.body.x + player.body.width/2;
    playerVirtual.body.y=player.body.y + player.body.height/2;
    playerVirtual.body.angle = player.angle;

    playerVirtualCollision.body.x=player.body.x + player.body.width/2;
    playerVirtualCollision.body.y=player.body.y + player.body.height/2;
    playerVirtualCollision.body.angle = player.angle;

    if(player.body.x>land[land.length-1].x){
    } 
    if(powerTimer.getTimeValues().seconds>=5 && !collided){
        /* END OF POWER*******/
        if(!loadingPower){
            powerAudio.stop();
            $(".power-loading span").animate({width:"100%"},{duration:7000, easing: "linear"});
            $(".power-loading span").addClass("disabled");
            $(".power-box .box-icon").addClass("disabled");
            loadingPower=true;
            ACCLERATION = 700;
            DRAG = 800;
            mainVelocity = 2;
            setTimeout(function(){
                powerAvailable=true; 
                powerTimer.reset();
                powerTimer.stop();
                $(".power-box p").show();
                $(".power-box .box-icon").addClass("enabled"); 
                $(".power-loading span").removeClass("disabled"); 
                $(".power-box .box-icon").removeClass("disabled");
            },7000);
        }
    }
    if(powerAvailable || loadingPower){
        if(!endedGame){
            player.y=player.y-((Math.random()-0.5) *3);
        }
    }
    if(!collided){
        moveBackGround(mainVelocity);
        moveBlackWall(mainVelocity*2);
        moveItems(mainVelocity*2);
    }
    if(!collided && !collisionAlert){
        setTimeout(function(){
            collisionAlert=true;
            $(".img-alert").removeClass("visible");
            alertAudio.stop();
        },1000);
    }
}
function powerActivate(){
    if(powerAvailable)
    {
        powerAudio.loopFull();        
        ACCLERATION = 800;
        DRAG = 900;
        mainVelocity = 1.8;
        loadingPower=false;
        $(".power-loading span").animate({width:0},{duration:5000, easing: "linear"});
        $(".power-box .box-icon").removeClass("enabled");
        $(".power-box p").fadeOut(300);
        powerTimer.start();
        powerAvailable=false;
    }
}
function collision(body1, body2){   
    if(!endedGame){
        collisionAlert=false;
        if(body1.sprite.key=="shipvirtualcollision" || body2.sprite.key=="shipvirtualcollision")
        {
            if(!alertAudio.isPlaying){
                $(".img-alert").addClass("visible");
                alertAudio.play();
            }
        }
        else{        
        }
        if(body2.sprite.key=="shipvirtual"){
            mainVelocity=0;
            collided=true;
            //collisione effettiva
            $(".img-alert").removeClass("visible");
            var explosion = explosions.getFirstExists(false);
            explosion.reset(player.body.x, player.body.y);
            explosion.play('kaboom', 7, false, true);
            stopAllThings();
        } 
    }
}
function stopAllThings(){
    explosionAudio.play();
    setTimeout(function(){game.paused=true; timer.stop(); $(".crush-page").fadeIn(300);},2500);
    player.kill();
    playerVirtual.kill();
    playerVirtualCollision.kill();
    $(".power-loading span").stop();
    $(".power-box .box-icon").removeClass("enabled");
    
}
function timeIsOver(){
    //run out time
    
}
function keyDown(e) {
    if(e.detail.pressed){
        if($(".splash-page").is(":visible")){
            splashClick();
        }else if($(".istructions-page").is(":visible")){
            startClick();
        }else if(!game.paused){
            powerActivate();
        }else if($(".mission-complete-page").is(":visible")){
            rankingClick();
        }else if($(".ranking-page").is(":visible")){
            location.reload();
        }else if($(".crush-page").is(":visible")){
            location.reload();
        }else if($(".time-is-over-page").is(":visible")){
            location.reload();
        }
    }
}
function gamePadDirection(e) {
    var axis = e.detail.axis, value = e.detail.value;
    due="";
    if(axis=="0") //asse sx-dx
    {
        if(value=="1")
        {
            //spostamento a dx
            due = "RIGHT";
        }
        else if(value=="-1")
        {
            //spostamento a sx
            due = "LEFT";
        }
    }
    else if(axis="1") //asse su-giu
    {
        if(value=="-1")
        {
            //spostamento su
            due = "UP";
        }
        else if(value=="1")
        {
            //spostamento giu
            due = "DOWN";
        }
    }
}
function scalePolygon(originalPhysicsKey, newPhysicsKey, shapeKey, scale){
    var newData = [];
    var data = this.game.cache.getPhysicsData(originalPhysicsKey, shapeKey);
 
    for (var i = 0; i < data.length; i++) {
        var vertices = [];
 
        for (var j = 0; j < data[i].shape.length; j += 2) {
           vertices[j] = data[i].shape[j] * scale;
           vertices[j+1] = data[i].shape[j+1] * scale; 
        }
 
        newData.push({shape : vertices});
    }
 
    var item = {};
    item[shapeKey] = newData;
    game.load.physics(newPhysicsKey, '', item); 
 }
 function traslatePolygon(originalPhysicsKey, newPhysicsKey, shapeKey, counter){
    var newData = [];
    var data = this.game.cache.getPhysicsData(originalPhysicsKey, shapeKey);
    for (var i = 0; i < data.length; i++) {
        var vertices = [];
        for (var j = 0; j < data[i].shape.length; j += 2) {
            vertices[j] = data[i].shape[j];
            vertices[j+1] = data[i].shape[j+1];
        }
        newData.push({shape : vertices});
    }    
    var item = {};
    item[shapeKey] = newData;
    game.load.physics(newPhysicsKey, '', item);
 
 }
function render() {

}

